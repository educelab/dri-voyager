# Mesh Preparation

This document describes the general process for preparing a mesh so it can be
loaded by Smithsonian Voyager. Not all meshes will require all steps, so make
sure you understand the state of your starting mesh before running all of the
commands in this document.

## Prerequisites
The following applications are needed to run all of the commands in this
document.
- MeshLab
- ImageMagick
- [obj2gltf](https://github.com/CesiumGS/obj2gltf)
- [gltf-pipeline](https://github.com/CesiumGS/gltf-pipeline)

### macOS
```shell
# Install homebrew deps
brew install imagemagick meshlab node

# Install the other deps using node
npm install -g obj2gltf gltf-pipeline
```

## Convert mesh to an OBJ
If the mesh file is not an OBJ file, convert it to one using MeshLab:
1. Open the mesh in MeshLab
2. Go to `File->Export As...`
3. In the file selection dialog, enter a new output filename and select `Alias
Wavefront Object (*.obj)` from the filetype selection dropdown. Click Save.
4. In the file options dialog, make sure the `Normal` option is checked in the
`Vert` properties list.
5. Click OK to save.

## Generate mesh normals
If the OBJ file doesn't have surface normals, then Smithsonian Voyager will
display a black surface. Use one of the following methods to generate mesh
normals.

### MeshLab
MeshLab automatically generates per-vertex normals when you open a mesh. To add
normals to your mesh:
1. Open the mesh in MeshLab
2. Go to `File->Export...` or `File->Export As...`. The former option will **overwrite** the original mesh.
3. **(Export As only)** In the file selection dialog, enter a new output filename and select `Alias Wavefront Object (*.obj)` from the filetype selection dropdown. Click Save.
4. In the file options dialog, make sure the `Normal` option is checked in the `Vert` properties list.
5. Click OK to save.

## Texture image size and format
The `.glb` mesh format used by Smithsonian Voyager only supports JPEG or PNG
texture images. Additionally, current browsers largely only support WebGL
texture images with max dimensions of 8192x8192. Using ImageMagick, we can
convert our texture image to fit both of these constraints with one command:
```shell
convert -resize 8192x8192 texture.tif texture.png
```

Next, open the OBJ's `.mtl` file in your favorite plain text editor. Change the
`map_Kd` line to point to the new `.png` file.

## Convert the mesh to a glb
First convert the `.obj` to a `.glb`, then compress with DRACO
compression:
```shell
# Convert to glb
obj2gltf -i input_mesh.obj -o input_mesh.glb

# Compress with DRACO
gltf-pipeline -i input_mesh.glb -o input_mesh_compressed.glb -d
```
