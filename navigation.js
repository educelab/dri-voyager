// handle for window url
const url = new URL(window.location);
if (!url.searchParams.has("document")) {
    url.searchParams.set("document", "PHerc0089Cr001.json");
    window.history.replaceState({
        "document": "PHerc0089Cr001.json"
    }, "", url);
}

// populate the navigation bar with data
function navPopulate(data) {
    // Get the navigation list element and empty it
    let list = document.getElementById("nav-list-wrapper");
    while (list.firstChild) {
        list.removeChild(list.firstChild);
    }

    // Populate the list
    for (const d of data) {
        // Handle single-line documents: {"title": str, "document": str}
        if ("document" in d) {
            navAddDoc(list, d);
        }
        // Handle items with sub-documents: {"title" : str, "subitems": [...]}
        else if ("subitems" in d) {
            // Create top-level item
            let item = document.createElement("nav-group");
            list.appendChild(item);

            // Create title sub-element
            let a = document.createElement("nav-title");
            a.appendChild(document.createTextNode(d["title"]));
            a.title = d["title"];
            a.setAttribute("onClick", "navSubListOpen(this)");
            item.appendChild(a);

            // Create sub-list for subitems
            let sublist = document.createElement("nav-sublist");
            sublist.classList.add("hidden");
            for (const y of d["subitems"]) {
                navAddDoc(sublist, y);
            }
            item.appendChild(sublist);
        }
    }
}

// add a document selector to the provided navigation element
function navAddDoc(list, doc) {
    let item = document.createElement("nav-item");
    item.append(document.createTextNode(doc["title"]));
    item.title = doc["title"];
    item.setAttribute("document", doc["document"]);
    item.setAttribute("onClick", "selectDocBtn(this)");
    list.appendChild(item);
}

// open the sidebar
function navOpen() {
    document.getElementById("nav-list-wrapper").classList.remove("hidden");
    document.getElementById("nav-title").classList.remove("hidden");
    document.getElementById("nav-list-wrapper").classList.add("visible");
    document.getElementById("nav-title").classList.add("visible");
    document.getElementById("nav-bar").removeAttribute("style");
    document.getElementById("nav-title").removeAttribute("style");
    document.getElementById("nav-list").removeAttribute("style");
    document.getElementById("nav-btn").onclick = navClose;
}

// close the sidebar
function navClose() {
    document.getElementById("nav-list-wrapper").classList.remove("visible");
    document.getElementById("nav-title").classList.remove("visible");
    document.getElementById("nav-list-wrapper").classList.add("hidden");
    document.getElementById("nav-title").classList.add("hidden");
    document.getElementById("nav-bar").style.width = "54px";
    document.getElementById("nav-bar").style.minWidth = "54px";
    document.getElementById("nav-title").style.width = "0";
    document.getElementById("nav-list").style.width = "0";
    document.getElementById("nav-btn").onclick = navOpen;
}

// handle top-level item open
function navSubListOpen(btn) {
    let elem = btn.nextElementSibling;
    elem.classList.remove("hidden");
    elem.classList.add("visible");
    btn.classList.add("selected");
    btn.setAttribute("onClick", "navSubListClose(this)");
}

// handle top-level item close
function navSubListClose(btn) {
    let elem = btn.nextElementSibling;
    elem.classList.remove("visible");
    elem.classList.add("hidden");
    btn.classList.remove("selected");
    btn.setAttribute("onClick", "navSubListOpen(this)");
}

// set the current doc in the navigation bar
function navSetCurrentDoc(doc) {
    let query = "nav-item[document][document=\"" + doc + "\"]";
    let navItem = document.querySelector(query);
    if (navItem) {
        let oldDocBtn = document.getElementById("current-doc");
        if (oldDocBtn) {
            oldDocBtn.removeAttribute("id");
        }
        navItem.id = "current-doc";
        let parent = navItem.parentNode;
        if (parent.nodeName === "NAV-SUBLIST") {
          navSubListOpen(parent.previousElementSibling);
        }
    }
}

// select document button handler
function selectDocBtn(docBtn) {
    setCurrentDoc(docBtn.getAttribute("document"));
}

// set the current document
// if pushState is true, this action adds an entry to the history
function setCurrentDoc(doc, pushState = true) {
    // Skip if the new doc is the current doc
    let oldDocBtn = document.getElementById("current-doc");
    if (oldDocBtn && doc === oldDocBtn.getAttribute("document")) {
        return;
    }

    // Add the new doc to the history state
    url.searchParams.set("document", doc);
    if (pushState) {
        window.history.pushState({
            "document": doc
        }, "", url);
    }

    // Open nav bar to item and set as selected
    navSetCurrentDoc(doc);

    // Load the doc in explorer
    let explorer = document.getElementById("explorer");
    explorer.setAttribute("document", doc);
}

// load the data manifest and populate
fetch("data/items.json")
    .then(response => response.json())
    .then(data => {
        navPopulate(data);
    })
    .then(function() {
        // navigate to a document
        if (url.searchParams.has("document")) {
            setCurrentDoc(url.searchParams.get("document"), false);
        } else {
            setCurrentDoc("PHerc0089Cr001.json", false);
        }
    });

// Handle auto-closing the nav bar on window resizes
let LAST_WIDTH = window.innerWidth;
window.addEventListener("resize", function() {
    if (LAST_WIDTH >= 800 && window.innerWidth < 800) {
        navClose();
    }
    if (LAST_WIDTH < 800 && window.innerWidth >= 800) {
        navOpen();
    }
    LAST_WIDTH = window.innerWidth;
});

// Handle popstate
window.addEventListener('popstate', (event) => {
    if (event.state) {
        setCurrentDoc(event.state.document, false);
    }
});
